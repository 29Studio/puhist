@echo off
setlocal enabledelayedexpansion

set input_file=data.txt
set output_file=output.txt

REM ������� ��������� ����� ����� �������
echo. > %output_file%

REM ������ � ��������� ������ �� �������� �����
for /f "tokens=*" %%a in (%input_file%) do (
    set line=%%a
    REM ���������� ��������� ��� ������ ������ ������
    echo Processed: !line! >> %output_file%
)

echo Done! ������������ ������ �������� � %output_file%.

endlocal